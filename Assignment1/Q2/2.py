# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/god/.spyder2/.temp.py
"""
import numpy;
import pylab;

p=input('Enter the number of points into which the domain is to be discretised ')
lo=input('Enter the lower limit of the domain ')
up=input('Enter the upper limit of the domain ')
t=input('Enter ΔT/ΔX ')
u = numpy.zeros(p) #density function of the conserved parameter
ubuffer = numpy.zeros(p) #buffer function used to make the array operations easier to handle and understand
f = numpy.zeros(p) #flux function of the conserved quantity
phi = numpy.zeros(p-1) #phi function - difference in fluxes at consecutive points
sigma = numpy.zeros(p-1) #sigma array where sigma = Δf/Δu
x = numpy.linspace(lo,up,p) #points defined
signal = numpy.zeros(p-1)  #signal array defined : signal = (ΔT/ΔX)sigma
time=0.0
dt=t*(x[2]-x[1]) #difference in time after which each iteration is carried out(ie. snapshot view of the system is taken)

for i in range(0,p) :
   if (float(x[i]) > -0.333 and float(x[i]) < 0.333): #initial values defined
       u[i]=1.0
       f[i]=(u[i]**2)/2   #flux = (u^2)/2 for the given question   
       ubuffer[i]=u[i] #values of u inserted into ubuffer 
pass;

v=1
pylab.plot(x,u) #to plot the initial consition u vs x     
pylab.title("")
pylab.xlabel("(x) -->");
pylab.ylabel("(u) -->");
pylab.savefig("Plot"+str(v))
pylab.cla()

while time<0.6:

    for j in range(0,p-1):
           phi[j]=f[j+1]-f[j]
           sigma[j]=(u[j]+u[j+1])/2 #sigma = Δf/Δu simplified to get sigma = (u[j]+u[j+1])/2
           signal[j]=t*phi[j]
           if sigma[j]>0 :
               ubuffer[j+1]=u[j+1]-signal[j]  #flux difference algorithm used
           else :
               ubuffer[j]=ubuffer[j]-signal[j]  #flux difference algorithm used
    
    for k in range (0,p):    
        u[k]=ubuffer[k] #value of u updated
        f[k]=(u[k]**2)/2  #value of f updated  
    
    v+=1
    pylab.plot(x,u) #updated value of u array to be plotted     
    pylab.title("")
    pylab.xlabel("(x) -->");
    pylab.ylabel("(u) -->");
    pylab.savefig("Plot"+str(v))
    pylab.cla()
        
        
    time=time+dt

       
       
       
   
