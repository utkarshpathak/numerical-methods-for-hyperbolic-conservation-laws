# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/god/.spyder2/.temp.py
"""
import numpy;
import pylab;

p=input('Enter the number of points into which the domain is to be discretised ')
lo=input('Enter the lower limit of the domain ')
up=input('Enter the upper limit of the domain ')
t=input('Enter ΔT/ΔX ')
u = numpy.zeros(p) #density function of the conserved parameter
ubuffer = numpy.zeros(p) #buffer function used to make the array operations easier to handle and understand
f = numpy.zeros(p) #flux function of the conserved quantity 
phi = numpy.zeros(p-1) #phi function - difference in fluxes at consecutive points
sigma = numpy.zeros(p-1) #sigma array where sigma = Δf/Δu
x = numpy.linspace(lo,up,p) #points defined
signal = numpy.zeros(p-1) #signal array defined : signal = (ΔT/ΔX)sigma
time=0.0
dt=t*(x[2]-x[1]) #difference in time after which each iteration is carried out(ie. snapshot view of the system is taken)

for i in range(0,p) :
   if (float(x[i]) > -0.333 and float(x[i]) < 0.333):  #initial values defined
       u[i]=1.0
       f[i]=u[i] #flux = u for the given question     
pass;

v=1

pylab.plot(x,u) #to plot the initial consition u vs x  
pylab.title("")
pylab.xlabel("(x) -->");
pylab.ylabel("(u) -->");
pylab.savefig("Plot"+str(v))
pylab.cla()

while time<=4: #iteration to be carried out until 4 seconds

    for s in range(0,p): 
           ubuffer[s]=u[s] #values of u inserted into ubuffer    
    
    for j in range(0,p-1):
           phi[j]=f[j+1]-f[j]
           sigma[j]=1 #sigma taken as 1 as Δf/Δu is in the form of 0/0
           signal[j]=t*phi[j]   
           
    for d in range(0,p):           
           if d<(p-1) :             
             ubuffer[d+1]=u[d+1]-signal[d] #flux difference algorithm used
           else :
             ubuffer[0]=u[d] #statement to make the wave (ie. solution) periodic in nature   
    
    for k in range (0,p):    
        u[k]=ubuffer[k] #value of u updated
        f[k]=u[k] #value of f updated    
    
    v+=1
    pylab.plot(x,u)  #updated value of u array to be plotted 
    pylab.title("")
    pylab.xlabel("(x) -->");
    pylab.ylabel("(u) -->");
    pylab.savefig("Plot"+str(v))
    pylab.cla()
        
        
    time=time+dt 

       
       
       
   
