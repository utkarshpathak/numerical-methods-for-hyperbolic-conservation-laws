#Problem part 4 : HLL(E) method

import numpy;
import matplotlib.pyplot as pt;

#Declaring initial parameters :

n=input('Please input the number of points ')
gamma=1.4
lo=0.0
up=1.0
x = numpy.linspace(lo,up,n) 
dx=x[1]-x[0]
dt=0
t=0

u = numpy.zeros(n) 
p = numpy.zeros(n) 
rho = numpy.zeros(n)

e = numpy.zeros(n)
h = numpy.zeros(n)
a = numpy.zeros(n)
lmda = numpy.zeros((3,n))

umat = numpy.zeros((3,n)) 
fmat = numpy.zeros((3,n)) 
ucap = numpy.zeros(n-1) 
hcap = numpy.zeros(n-1) 
acap = numpy.zeros(n-1)
lmdacap = numpy.zeros((3,n-1))
fdiff = numpy.zeros((3,n-1))
smin=numpy.zeros(n-1)
smax=numpy.zeros(n-1)

#Inserting the initial values for the given parameters at all points:

for z in range(0,n) :
   if (float(x[z]) <= 0.300):
       u[z]= 0.75
       p[z]= 1             
       rho[z]= 1
   else:
       u[z]= 0
       p[z]= 0.1             
       rho[z]= 0.125       
pass;

#Looping for updation of conserved parameters over time

while (t<0.25):
    for z in range(0,n) : #Finding E,H,a at all half-junctions
        e[z]=((p[z])/(gamma-1))+(0.5*u[z]*u[z]*rho[z])
        h[z]=(e[z]+p[z])/rho[z]
        a[z]=(((gamma-1)*(h[z]-0.5*u[z]*u[z]))**0.5)
    pass;
    
    for z in range(0,n) : #Declaring the matrix U and matrix F
        umat[0,z]=rho[z]
        umat[1,z]=rho[z]*u[z]
        umat[2,z]=e[z]
        fmat[0,z]=rho[z]*u[z]
        fmat[1,z]=(rho[z]*u[z]*u[z])+p[z]
        fmat[2,z]=(e[z]+p[z])*u[z]
    pass;
    
    for z in range(0,n-1) : #Finding Roe averaged U,H and a at all half-junctions
        ucap[z]=((rho[z]**0.5)*(u[z])+(rho[z+1]**0.5)*(u[z+1]))/((rho[z]**0.5)+(rho[z+1]**0.5))
        hcap[z]=((rho[z]**0.5)*(h[z])+(rho[z+1]**0.5)*(h[z+1]))/((rho[z]**0.5)+(rho[z+1]**0.5))
        acap[z]=(((gamma-1)*(hcap[z]-0.5*ucap[z]*ucap[z]))**0.5)
    pass;
    
    for z in range(0,n-1) : #Finding Roe averaged lambda values at all half-junctions
        lmdacap[0,z]=(ucap[z]-acap[z])
        lmdacap[1,z]=(ucap[z])
        lmdacap[2,z]=(ucap[z]+acap[z])
    pass;
    
    for z in range(0,n) : #Finding lambda values at all points
        lmda[0,z]=(u[z]-a[z])
        lmda[1,z]=(u[z])
        lmda[2,z]=(u[z]+a[z])
    pass;
            
    for z in range(0,n-1) : #Finding flux at all half-junctions
        
        #Algorithm specific to HLL(E) method for finding flux at all half-junctions :
        smin1=min(lmda[:,z])
        smin2=min(lmdacap[:,z])           
        smin[z]=min(smin1,smin2)
        smax1=max(lmda[:,z+1])
        smax2=max(lmdacap[:,z])
        smax[z]=max(smax1,smax2)
        
        if smin[z]>0 :
            fdiff[0,z]=fmat[0,z]
            fdiff[1,z]=fmat[1,z] 
            fdiff[2,z]=fmat[2,z]
        if smin[z]<0 and smax[z]>0 :    
            fdiff[0,z]=(smax[z]*fmat[0,z]-smin[z]*fmat[0,z+1]+smax[z]*smin[z]*(umat[0,z+1]-umat[0,z]))/(smax[z]-smin[z])                    
            fdiff[1,z]=(smax[z]*fmat[1,z]-smin[z]*fmat[1,z+1]+smax[z]*smin[z]*(umat[1,z+1]-umat[1,z]))/(smax[z]-smin[z])
            fdiff[2,z]=(smax[z]*fmat[2,z]-smin[z]*fmat[2,z+1]+smax[z]*smin[z]*(umat[2,z+1]-umat[2,z]))/(smax[z]-smin[z])
        if smax[z]<0 :
            fdiff[0,z]=fmat[0,z+1]
            fdiff[1,z]=fmat[1,z+1]
            fdiff[2,z]=fmat[2,z+1]       
    pass;
   
    #Finding the maximum lambda value in the system to find the appropriate time step:
    alimit=max(max(abs(lmda[0,:])),max(abs(lmda[1,:])),max(abs(lmda[2,:])))
   
    #Finding the maximum possible time step that can be considered for value updation:
    dt=0.8*dx/alimit
    
    for z in range(0,n-2) : #Updating the matrix U
        umat[0,z+1]=umat[0,z+1]-(dt/dx)*((fdiff[0,z+1])-(fdiff[0,z]))
        umat[1,z+1]=umat[1,z+1]-(dt/dx)*((fdiff[1,z+1])-(fdiff[1,z]))
        umat[2,z+1]=umat[2,z+1]-(dt/dx)*((fdiff[2,z+1])-(fdiff[2,z]))
    pass;
    
    for z in range(0,n): #Updating the paramters rho, u and p at all points
        rho[z]=umat[0,z]
        u[z]=umat[1,z]/rho[z]
        p[z]=(gamma-1)*(umat[2,z]-0.5*rho[z]*u[z]*u[z])
    
    t=t+dt #Adding the time step to total time     

#Plotting:
pt.plot(x,p,label='pressure')
pt.plot(x,rho,label='density')
pt.plot(x,u,label='velocity')
pt.legend(loc='best')
pt.title(str("HLL(E) method with")+" no. of points = "+str(n))
pt.savefig(str("HLL(E) method")+"&"+str(n)+" points")
pt.show()