#Problem part 1 : LLF method

import numpy;
import matplotlib.pyplot as pt;

#Declaring initial parameters :

n=input('Please input the number of points ')
gamma=1.4
lo=0.0
up=1.0
x = numpy.linspace(lo,up,n) 
dx=x[1]-x[0]
dt=0
t=0

u = numpy.zeros(n) 
p = numpy.zeros(n) 
rho = numpy.zeros(n)

e = numpy.zeros(n)
h = numpy.zeros(n)
a = numpy.zeros(n)
lmda = numpy.zeros((3,n))

umat = numpy.zeros((3,n)) 
fmat = numpy.zeros((3,n)) 
ucap = numpy.zeros(n-1) 
hcap = numpy.zeros(n-1) 
acap = numpy.zeros(n-1)
lmdacap = numpy.zeros((3,n-1))
r1cap = numpy.zeros((3,n-1))
r2cap = numpy.zeros((3,n-1))
r3cap = numpy.zeros((3,n-1))
delta = numpy.zeros((3,n-1))
alpha = numpy.zeros((3,n-1))
fdiff = numpy.zeros((3,n-1))

#Inserting the initial values for the given parameters at all points:

for z in range(0,n) :
   if (float(x[z]) <= 0.300):
       u[z]= 0.75
       p[z]= 1             
       rho[z]= 1
   else:
       u[z]= 0
       p[z]= 0.1             
       rho[z]= 0.125       
pass;

#Looping for updation of conserved parameters over time

while (t<0.25):
    for z in range(0,n) : #Finding E,H,a at all half-junctions
        e[z]=((p[z])/(gamma-1))+(0.5*u[z]*u[z]*rho[z])
        h[z]=(e[z]+p[z])/rho[z]
        a[z]=(((gamma-1)*(h[z]-0.5*u[z]*u[z]))**0.5)
    pass;
    
    for z in range(0,n) : #Declaring the matrix U and matrix F
        umat[0,z]=rho[z]
        umat[1,z]=rho[z]*u[z]
        umat[2,z]=e[z]
        fmat[0,z]=rho[z]*u[z]
        fmat[1,z]=(rho[z]*u[z]*u[z])+p[z]
        fmat[2,z]=(e[z]+p[z])*u[z]
    pass;
    
    for z in range(0,n-1) : #Finding Roe averaged U,H and a at all half-junctions
        ucap[z]=((rho[z]**0.5)*(u[z])+(rho[z+1]**0.5)*(u[z+1]))/((rho[z]**0.5)+(rho[z+1]**0.5))
        hcap[z]=((rho[z]**0.5)*(h[z])+(rho[z+1]**0.5)*(h[z+1]))/((rho[z]**0.5)+(rho[z+1]**0.5))
        acap[z]=(((gamma-1)*(hcap[z]-0.5*ucap[z]*ucap[z]))**0.5)
    pass;
    
    for z in range(0,n-1) : #Finding Roe averaged lambda values at all half-junctions
        lmdacap[0,z]=(ucap[z]-acap[z])
        lmdacap[1,z]=(ucap[z])
        lmdacap[2,z]=(ucap[z]+acap[z])
    pass;
    
    for z in range(0,n) : #Finding lambda values at all points
        lmda[0,z]=(u[z]-a[z])
        lmda[1,z]=(u[z])
        lmda[2,z]=(u[z]+a[z])
    pass;
    
    for z in range(0,n-1) : #Declaring Roe averaged r1,r2,r3 eigen vectors at all half-junctions
        r1cap[0,z]=1.0
        r1cap[1,z]=(ucap[z]-acap[z])
        r1cap[2,z]=(hcap[z]-(ucap[z]*acap[z]))
        r2cap[0,z]=1.0
        r2cap[1,z]=(ucap[z])
        r2cap[2,z]=(ucap[z]*ucap[z]*0.5) 
        r3cap[0,z]=1.0
        r3cap[1,z]=(ucap[z]+acap[z])
        r3cap[2,z]=(hcap[z]+(ucap[z]*acap[z]))
    pass;
    
    for z in range(0,n-1) : #Finding delta matrix for finding alpha values later
        delta[0,z]=(umat[0,z+1]-umat[0,z])
        delta[1,z]=(umat[1,z+1]-umat[1,z])
        delta[2,z]=(umat[2,z+1]-umat[2,z])
    pass;
    
    for z in range(0,n-1) : #Finding alpha values
        alpha[1,z]=(gamma-1)*((((hcap[z]-ucap[z]*ucap[z])*(delta[0,z]))+(ucap[z]*delta[1,z])-delta[2,z])/(acap[z]*acap[z]))
        alpha[2,z]=((delta[1,z])+(acap[z]-ucap[z])*(delta[0,z])-(acap[z])*(alpha[1,z]))/(2*acap[z])
        alpha[0,z]=delta[0,z]-alpha[1,z]-alpha[2,z]
    pass;
        
    for z in range(0,n-1) : #Finding flux at all half-junctions
        fdiff[0,z]=((fmat[0,z]+fmat[0,z+1])/2)-0.5*((((max(abs(lmda[0,z]),abs(lmda[0,z+1])))*(alpha[0,z])*(r1cap[0,z]))+((max(abs(lmda[1,z]),abs(lmda[1,z+1])))*(alpha[1,z])*(r2cap[0,z]))+((max(abs(lmda[2,z]),abs(lmda[2,z+1])))*(alpha[2,z])*(r3cap[0,z]))))
        fdiff[1,z]=((fmat[1,z]+fmat[1,z+1])/2)-0.5*((((max(abs(lmda[0,z]),abs(lmda[0,z+1])))*(alpha[0,z])*(r1cap[1,z]))+((max(abs(lmda[1,z]),abs(lmda[1,z+1])))*(alpha[1,z])*(r2cap[1,z]))+((max(abs(lmda[2,z]),abs(lmda[2,z+1])))*(alpha[2,z])*(r3cap[1,z]))))
        fdiff[2,z]=((fmat[2,z]+fmat[2,z+1])/2)-0.5*((((max(abs(lmda[0,z]),abs(lmda[0,z+1])))*(alpha[0,z])*(r1cap[2,z]))+((max(abs(lmda[1,z]),abs(lmda[1,z+1])))*(alpha[1,z])*(r2cap[2,z]))+((max(abs(lmda[2,z]),abs(lmda[2,z+1])))*(alpha[2,z])*(r3cap[2,z]))))
    pass;
    
    #Finding the maximum lambda value in the system to find the appropriate time step:
    alimit=max(max(abs(lmda[0,:])),max(abs(lmda[1,:])),max(abs(lmda[2,:])))
    
    #Finding the maximum possible time step that can be considered for value updation:
    dt=0.8*dx/alimit
    
    for z in range(0,n-2) : #Updating the matrix U
        umat[0,z+1]=umat[0,z+1]-(dt/dx)*((fdiff[0,z+1])-(fdiff[0,z]))
        umat[1,z+1]=umat[1,z+1]-(dt/dx)*((fdiff[1,z+1])-(fdiff[1,z]))
        umat[2,z+1]=umat[2,z+1]-(dt/dx)*((fdiff[2,z+1])-(fdiff[2,z]))
    pass;
    
    for z in range(0,n): #Updating the paramters rho, u and p at all points
        rho[z]=umat[0,z]
        u[z]=umat[1,z]/rho[z]
        p[z]=(gamma-1)*(umat[2,z]-0.5*rho[z]*u[z]*u[z])
    
    t=t+dt #Adding the time step to total time   


#Plotting:
pt.plot(x,p,label='pressure')
pt.plot(x,rho,label='density')
pt.plot(x,u,label='velocity')
pt.legend(loc='best')
pt.title(str("LLF method with")+" no. of points = "+str(n))
pt.savefig(str("LLF method")+"&"+str(n)+" points")
pt.show()