# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/god/.spyder2/.temp.py
"""
import numpy;
import pylab;

o=200
lo=0.0
up=1.0
t=0.8
u = numpy.zeros(o) 
p = numpy.zeros(o) 
ubuffer = numpy.zeros(o) #buffer function used to make the array operations easier to handle and understand
pbuffer = numpy.zeros(o) #buffer function used to make the array operations easier to handle and understand
x = numpy.linspace(lo,up,o) #points defined
pfluxp = numpy.zeros(o-1)
pfluxn = numpy.zeros(o-1)
ufluxp = numpy.zeros(o-1)
ufluxn = numpy.zeros(o-1)
rho = numpy.zeros(o)
time=0.0
co = numpy.zeros(o)
zo = numpy.zeros(o)
dt=0.004 #difference in time after which each iteration is carried out(ie. snapshot view of the system is taken)

for i in range(0,o) :
   if (float(x[i]) > 0.325 and float(x[i]) < 0.475):  #initial values defined
       u[i]=0.0
       p[i]=0.2*((1-((x[i]-0.4)/0.075)**2)**0.5)
              
pass;

for g in range(0,o) :
   rho[g]=1.0   
   if (float(x[g]) > 0.6):  #initial values defined
       rho[g]=3.0
   co[g] = (1 / rho[g])**0.5
   zo[g] = rho[g] * co[g]
        
pass;

v=1

pylab.plot(x,u) #to plot the initial consition u vs x  
pylab.title("")
pylab.xlabel("(x) -->");
pylab.ylabel("(u) -->");
pylab.savefig("UPlot"+str(v))
pylab.cla()

pylab.plot(x,p) #to plot the initial consition p vs x  
pylab.title("")
pylab.xlabel("(x) -->");
pylab.ylabel("(p) -->");
pylab.savefig("PPlot"+str(v))
pylab.cla()

while time<=0.6: #iteration

    for s in range(0,o): 
           ubuffer[s]=u[s] #values of u inserted into ubuffer   
           pbuffer[s]=p[s]
    
    for y in range(0,o-1): #flux splitting algorithm
          pfluxp[y]=(zo[y]*co[y])*((pbuffer[y+1]-pbuffer[y])+(zo[y])*(ubuffer[y+1]-ubuffer[y]))/(zo[y+1]+zo[y])
          pfluxn[y]=(zo[y]*co[y])*((pbuffer[y]-pbuffer[y+1])+(zo[y+1])*(ubuffer[y+1]-ubuffer[y]))/(zo[y+1]+zo[y])
          ufluxp[y]=(co[y])*((pbuffer[y+1]-pbuffer[y])+(zo[y])*(ubuffer[y+1]-ubuffer[y]))/(zo[y+1]+zo[y])     
          ufluxn[y]=(-co[y])*((pbuffer[y]-pbuffer[y+1])+(zo[y+1])*(ubuffer[y+1]-ubuffer[y]))/(zo[y+1]+zo[y])
    
    for j in range(0,o):
           if j!=0:
                if j!=o-1:
                    pbuffer[j] = pbuffer[j] - 0.8 * (pfluxp[j-1]+pfluxn[j])
                    ubuffer[j] = ubuffer[j] - 0.8 * (ufluxp[j-1]+ufluxn[j])
                if j==o-1:
                    ubuffer[j] = ubuffer[j] - 0.8 * (ufluxp[j-1])
                    pbuffer[j] = pbuffer[j] - 0.8 * (pfluxp[j-1])
                    
           if j==0:
                ubuffer[j] = ubuffer[j] - 0.8 * (ufluxn[j])
                pbuffer[j] = pbuffer[j] - 0.8 * (pfluxn[j])
           
    
    
    for k in range (0,o):    
        u[k]=ubuffer[k] #value of u updated
        p[k]=pbuffer[k]
            
    
    v+=1
    pylab.plot(x,u)  #updated value of u array to be plotted 
    pylab.title("")
    pylab.xlabel("(x) -->");
    pylab.ylabel("(u) -->");
    pylab.savefig("UPlot"+str(v))
    pylab.cla()
    
    pylab.plot(x,p)  #updated value of p array to be plotted 
    pylab.title("")
    pylab.xlabel("(x) -->");
    pylab.ylabel("(p) -->");
    pylab.savefig("PPlot"+str(v))
    pylab.cla()    
        
    time=time+dt 

       
       
       
   